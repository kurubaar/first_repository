package com.jpm.selenium.forms;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.jpm.selenium.util.ChromeUtil;

public class WorkingWithForms {

	public static void main(String[] args) throws InterruptedException {
		// load driver
		WebDriver driver = ChromeUtil.getChromeDriver();
		//open the webpage
		driver.get("file:\\D:\\Aruna\\Selenium-WS\\11_SeleniumWebdriver\\src\\main\\java\\WorkingWithForms.html");
		//maximize
		driver.manage().window().maximize();
		driver.findElement(By.id("txtUserName")).sendKeys("JPM_Aruna");
		driver.findElement(By.name("txtPwd")).sendKeys("jpm@123");//pwd
		driver.findElement(By.className("Format")).sendKeys("jpm@123");//confrim pwd
		driver.findElement(By.cssSelector("Input.Format1")).sendKeys("Aruna");//here .(dot) is defined in style.css file
		driver.findElement(By.cssSelector("Input#txtLastName")).sendKeys("Kuruba"); //here #(Hash) is defined in style.css file
		List<WebElement> radioElements=driver.findElements(By.name("gender"));
		for(WebElement radio: radioElements){
			String radioSelection = radio.getAttribute("value").toString();
			//which radio button to be selected
			if(radioSelection.equals("Female")){
				radio.click();
			}
		}
		driver.findElement(By.cssSelector("input[type=date]")).sendKeys("12/12/2001");
		driver.findElement(By.id("txtEmail")).sendKeys("aruna@gmail.com");
		
		driver.findElement(By.name("Address")).sendKeys("Hyderabad");;
		Select drpCity = new Select(driver.findElement(By.name("City")));
		//Dropdown 3 ways - > selectByIndex, selectByValue, selectByVisibleText
		drpCity.selectByIndex(2);
		drpCity.selectByValue("Hyderabad");
		drpCity.selectByVisibleText("Pune");
		driver.findElement(By.name("Phone")).sendKeys("8978795419");
		
		List<WebElement> checkEleList = driver.findElements(By.name("chkHobbies"));
		for (WebElement hobbyChkBox: checkEleList){
			String selection = hobbyChkBox.getAttribute("value").toString();
			if(!selection.equals("Movies")){//not click movies
				hobbyChkBox.click();
			}
		}
		Thread.sleep(5000);
		driver.close();
		System.out.println("Registration on process!");

	}

}
