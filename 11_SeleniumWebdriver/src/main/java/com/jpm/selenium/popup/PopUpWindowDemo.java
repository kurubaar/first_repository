package com.jpm.selenium.popup;

import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.jpm.selenium.util.ChromeUtil;

public class PopUpWindowDemo {

	public static void main(String[] args) throws InterruptedException {
		// load driver
		WebDriver driver = ChromeUtil.getChromeDriver();
		//open the web page
		String url = "File:\\D:\\Aruna\\Selenium-WS\\11_SeleniumWebdriver\\src\\main\\java\\PopUpWinDemo.html";
		driver.get(url);
		String parentWin = driver.getWindowHandle();
		System.out.println("Parent Win:"+parentWin);
		driver.manage().window().maximize();
		Thread.sleep(2000);
		driver.findElement(By.id("newtab")).click();
	    parentWin = driver.getWindowHandle();//parent window
		System.out.println("Parent Win:"+parentWin);
	
		Set<String> ChildWinList=driver.getWindowHandles();// all the window used
		for(String childWin : driver.getWindowHandles()){
			if(!childWin.equals(parentWin)){
				Thread.sleep(2000);
				driver.switchTo().window(childWin);
				System.out.println("Child Win:"+childWin);
				driver.findElement(By.id("alert")).click();
				Thread.sleep(2000);
				Alert alert = driver.switchTo().alert();
				Thread.sleep(2000);
				alert.accept();//use dismiss() method to cancel
			}
		}
		driver.switchTo().window(parentWin);
		System.out.println("Switching to Parent Win: "+parentWin);
		Thread.sleep(2000);
		System.out.println("Again click to another child win: ");
		driver.findElement(By.id("newwindow")).click();
		for(String childWin2: driver.getWindowHandles()){
			if(!childWin2.equals(parentWin)){
				driver.switchTo().window(childWin2);
				System.out.println("Child Window 2: "+childWin2);
				Thread.sleep(2000);
			}
		}
		 
		
		Thread.sleep(2000);
		driver.quit();
	}

}
