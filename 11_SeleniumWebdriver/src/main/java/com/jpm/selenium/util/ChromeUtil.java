package com.jpm.selenium.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ChromeUtil {
   
	public static WebDriver getChromeDriver(){
		WebDriver driver=null;
		//step1: driver class
		String driverClassKey ="webdriver.chrome.driver";
		//step 2: driver path ( we have kept driver in driver folder)
		String driverPath =".\\driver\\chromedriver.exe";
		//step 3: set the system class properties
		System.setProperty(driverClassKey, driverPath);
		//step 4: set the chrome options
		ChromeOptions options = new ChromeOptions();
		//step 5: get the chrome driver instance by passing chromeoptions
		driver = new ChromeDriver(options);
		System.out.println("Trying to load chrome browser");
		if(driver!=null){
			System.out.println("Driver loaded!!");
		}
		return driver;
	}
		
	public static void main(String[] args){
		
		WebDriver driver = ChromeUtil.getChromeDriver();
		System.out.println("Opening the browser");
		System.out.println("Driver : "+driver);
		System.out.println("Closing the Browser");
		driver.close();
		
		
	}
	//get
	//getCurrentUrl
	//findElement
	//getPageSource()
	//getTitle()
	//close()
	//quit() - close all the instance of browser created by WebDriver
	//getWindowHandles() - handling child window
	//switchTo() - switching between windows
	//navigate()- to do browser navigation - back,forward,to,refresh
	
}
